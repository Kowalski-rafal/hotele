var json = {
    "Warmińsko-Mazurskie": {
        "Elbląg": ["Młyn1", "Młyn2", "Młyn3"],
        "Ostróda": ["Wodnik1", "Wodnik2", "Wodnik3"],
        "Olsztyn": ["Novo1", "Novo2", "Novo3"]
    },
    "Pomorskie": {
        "Gdańsk": ["Hilton1", "Hilton2", "Hilton3"],
        "Gdynia": ["Leśny1", "Leśny2", "Leśny3"],
        "Sopot": ["Paris1", "Paris2", "Paris3"]
    },
    "Dolnośląskie": {
        "Wrocław": ["Wroclavia1", "Wroclavia2", "Wroclavia3"],
        "Wałbrzych": ["Kopalnia1", "Kopalnia2", "Kopalnia3"],
        "Oława": ["Olavia1", "Olavia2", "Olavia3"]
    }
}


window.onload = () => {

    const stateSelect = document.getElementById("stateSelect"),
        citySelect = document.getElementById("citySelect"),
        hotelsSelect = document.getElementById("hotelsSelect");

    for (const state in json) {
        stateSelect.options[stateSelect.options.length] = new Option(state, state);
    }

    stateSelect.onchange = function () {

        citySelect.length = 1;
        hotelsSelect.length = 1;

        if (this.selectedIndex < 1)
            return
        const x = this.options[this.selectedIndex].text;
        document.getElementById('yourChoiceState').innerHTML = "Województwo: " + x;
        console.log(x)

        for (var city in json[this.value]) {
            citySelect.options[citySelect.options.length] = new Option(city, city);
        }
    }

    citySelect.onchange = function () {
        hotelsSelect.length = 1;
        if (this.selectedIndex < 1)
            return
        const y = this.options[this.selectedIndex].text;
        document.getElementById('yourChoiceCity').innerHTML = "Miasto: " + y;
        console.log(y);

        const hotels = json[stateSelect.value][this.value];
        for (var i = 0; i < hotels.length; i++) {
            hotelsSelect.options[hotelsSelect.options.length] = new Option(hotels[i], hotels[i]);
        }

        hotelsSelect.onchange = function () {
            const z = this.options[this.selectedIndex].text;
            for (var y = 0; y < hotels.length; y++) {
                document.getElementById('yourChoiceHotels').innerHTML = "Hotel: " + z;
                console.log(y);
            }
        }
        document.getElementById('btn').addEventListener('click', function () {
            document.getElementById('showResult').style.display = 'block';
            document.getElementById('selectHotels').style.display = 'none';
            document.getElementById('btn').style.display = 'none';
        })
    }
}